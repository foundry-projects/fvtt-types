import * as recast from 'recast';

const n = recast.types.namedTypes;

/**
 * @type {Record<string, NodeHandler>}
 */
const handlers = {
	headHandler(node, context) {
		const isClass = n.ClassDeclaration.check(node);

		if (context.name === 'head' && !isClass) return true;
	},
	functionHandler(node, context, changeContext) {
		const isFuncDeclaration = n.FunctionDeclaration.check(node);

		if (isFuncDeclaration) {
			if (node.id.name === 'saveDataToFile') {
				changeContext(context, 'file');
				return true;
			}
			if (node.id.name === 'fromUuid') {
				changeContext(context, 'document');
				return true;
			}

			if (
				context.name === 'file' ||
				context.name === 'document' ||
				context.name === 'HandlebarsHelpers' ||
				context.name === 'TextureLoader'
			)
				return true;
		}
	},
	variableHandler(node, context, changeContext) {
		const isVarDeclaration = n.VariableDeclaration.check(node);

		if (isVarDeclaration) {
			const name = node.declarations[0].id.name;

			if (name === '_appId') {
				changeContext(context, 'Application');
				return true;
			}
			if (context.name === 'Application') return true;

			if (name === 'twist') return true;

			if (name === 'DicePool') return true;

			if (name === 'CanvasDocumentMixin') {
				changeContext(context, 'CanvasDocumentMixin');
				return true;
			}

			if (name === 'ClientDocumentMixin') {
				changeContext(context, 'ClientDocumentMixin');
				return true;
			}

			if (name === 'TabsV2') return true;

			if (name === '_token') return true;

			if (name === 'BLEND_MODES') {
				changeContext(context, 'BLEND_MODES');
				return true;
			}

			if (name === 'CONFIG') {
				changeContext(context, 'config');
				return true;
			}
		}
	},
	classHandler(node, context, changeContext) {
		const isClass = n.ClassDeclaration.check(node);

		if (isClass) {
			// if (node.superClass) {
			// 	const superName = node.superClass.name;

			// 	if (superName === 'SidebarTab') {
			// 		changeContext(context, node.id.name);
			// 		return true;
			// 	}

			// 	if (superName === 'SidebarDirectory') {
			// 		changeContext(context, node.id.name);
			// 		return true;
			// 	}
			// }

			if (context.name === node.id.name) {
				return true;
			} else {
				changeContext(context, node.id.name);
				return true;
			}
		}
	},
	expressionHandler(node, context, changeContext) {
		const isExpression = n.ExpressionStatement.check(node);

		if (isExpression) {
			const isAssignmentExpression = n.AssignmentExpression.check(
				node.expression
			);
			const isCallExpression = n.CallExpression.check(node.expression);
			const expression = node.expression;

			if (isAssignmentExpression) {
				let handled = false;
				recast.visit(node, {
					visitMemberExpression(path) {
						const node = path.node;

						if (node.object.name === context.name) {
							handled = true;
							return false;
						}

						this.traverse(path);
					},
				});

				if (handled) return true;

				if (expression.left.name === '_templateCache') {
					changeContext(context, 'HandlebarsHelpers');
					return true;
				}

				if (expression.left.property.name === 'TextEditor') return true;
			}

			if (isCallExpression) {
				if (context.name === 'HandlebarsHelpers') {
					return true;
				}
			}

			if (context.name === 'Application') {
				return true;
			}
		}
	},
	tailHandler(node, context, changeContext) {
		const nextIsForOf = n.ForOfStatement.check(node);

		if (context.name !== 'config' && context.name !== 'tail') return;

		if (context.name === 'tail') {
			return true;
		}

		if (nextIsForOf) {
			changeContext(context, 'tail');
			return true;
		}
	},
};

export default handlers;
