import * as recast from 'recast';

const n = recast.types.namedTypes;

/**
 * @type {Record<string, ConditionHandler>}
 */
const handlers = {
	forceChangeCondition(node, nextNode, context) {
		if (context.forceChange) {
			return context.newName;
		}
	},
	classCondition(node, nextNode, context) {
		const nextIsClass = n.ClassDeclaration.check(nextNode);

		if (nextIsClass) {
			const name = nextNode.id.name;
			return name;
		}
	},
};

export default handlers;
