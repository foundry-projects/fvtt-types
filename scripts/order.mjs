const CLIENT_SCRIPTS = [
	'client/head.js',
	'client/core/*.js',
	'client/apps/*.js',
	'client/game.js',
	'client/dice/*.js',
	'client/dice/terms/*.js',
	'client/dice/dice/*.js',
	'client/data/abstract/*.js',
	'client/data/collections/*.js',
	'client/data/documents/*.js',
	'client/data/embedded/*.js',
	'client/pixi/*.js',
	'client/ui/*.js',
	'client/apps/*.js',
	'client/apps/sidebar/*.js',
	'client/apps/forms/*.js',
	'client/apps/hud/*.js',

	'client/apps/**/*.js',

	'client/pixi/**/*.js',

	'client/av/*.js',

	'client/av/**/*.js',

	'client/**/*.js',

	'client/config.js', // Config comes last so all other classes are defined
	'client/tail.js',
	'!client/setup/**/*', // Exclude setup scripts
];
