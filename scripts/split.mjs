import * as recast from 'recast';
import * as babel from 'recast/parsers/babel.js';
import prettier from 'prettier';
import fs from 'fs-extra';
import path from 'path';
import consola from 'consola';
import chalk from 'chalk';
import _ from 'lodash';

import nHandlers from './nodeHandlers.mjs';

const b = recast.types.builders;

const source = fs.readFileSync(path.join(process.cwd(), 'foundry.js'), 'utf-8');
const savePath = path.join(process.cwd(), 'source');

const prettierConfig = fs.readJSONSync(path.join(process.cwd(), '.prettierrc'));

prettierConfig.parser = 'babel';

fs.rmSync(savePath, { recursive: true, force: true });
fs.mkdirSync(savePath);

consola.info('Generating AST...');

const ast = recast.parse(source, {
	parser: {
		parse(source) {
			return babel.parse(source);
		},
	},
});

consola.info('Processing nodes...');

/**
 * @type {NodeHandler[]}
 */
const nodeHandlers = Object.values(nHandlers);

const outputPaths = {
	unhandled: '/',
	head: '/',
	Hooks: 'core',
	Application: 'apps',

	Game: '/',

	Roll: 'dice',
	DiceTerm: 'dice/terms',
	Coin: 'dice/dice',

	CanvasDocumentMixin: 'data/abstract',
	DocumentCollection: 'data/collections',
	ActiveEffect: 'data/documents',
	TileDocument: 'data/embedded',

	Canvas: 'pixi',
	ContextMenu: 'ui',

	FormDataExtended: 'apps',
	Sidebar: 'apps/sidebar',
	ActorSheet: 'apps/forms',
	ChatBubbles: 'apps/hud',

	PlayerList: 'apps/misc',
	AVConfig: 'apps/config',
	CameraPopoutAppWrapper: 'apps/misc',

	DrawingConfig: 'apps/config',
	DrawingHUD: 'apps/hud',

	LightConfig: 'apps/config',
	TileHUD: 'apps/hud',

	TokenConfig: 'apps/config',
	TokenHUD: 'apps/hud',

	WallConfig: 'apps/config',
	ChatPopout: 'apps/misc',
	SettingsConfig: 'apps/config',
	Compendium: 'apps/misc',
	PermissionConfig: 'apps/config',

	ActorDirectory: 'apps/sidebar/directories',
	ChatLog: 'apps/sidebar/tabs',
	ItemDirectory: 'apps/sidebar/directories',
	Settings: 'apps/sidebar/tabs',

	FrameViewer: 'apps/misc',

	CanvasAnimation: 'pixi/misc',

	BackgroundLayer: 'pixi/layers',
	Drawing: 'pixi/placeables',
	AbstractBaseShader: 'pixi/shaders',
	Cursor: 'pixi/misc',
	ControlsLayer: 'pixi/layers',
	Ruler: 'pixi/misc',
	AutumnLeavesWeatherEffect: 'pixi/effects',
	BaseGrid: 'pixi/grid',
	GridLayer: 'pixi/layers',
	SquareGrid: 'pixi/grid',

	AVClient: 'av',
	config: '/',
	tail: '/',
	AudioHelper: 'sound',
};

let pathContext = '';

/**
 * @type {NodeContext}
 */
let nodeContext = {
	name: 'head',
	nodes: [],
	prettify: true,
};

const unhandledNodes = [];

/*
 * Iterate over all root AST nodes.
 */
for (let i = 0; i < ast.program.body.length; i++) {
	const node = ast.program.body[i];
	const nextNode = ast.program.body[i + 1];
	const nodeType = node.type;

	// Execute all handlers
	let handled = false;
	for (const handler of nodeHandlers) {
		const result = handler(node, nodeContext, changeContext);

		// If a handler returns true, consider the node resolved.
		if (result) {
			consola.success(`Handled node '${nodeType}'`);
			nodeContext.nodes.push(node);
			handled = true;
			break;
		}
	}

	// If the node has not been handled, save it for later.
	// To ensure no node is skipped and ignored.
	if (!handled) {
		consola.warn(`No handler for node '${nodeType}' found.`);
		unhandledNodes.push(node);
	}

	if (!nextNode) changeContext(nodeContext, null);
}

consola.success('Finished processing nodes.');

// If there are unhandled nodes, write them to a single unaltered file.
if (unhandledNodes.length > 0) {
	pathContext = outputPaths['unhandled'];
	nodeContext = {
		name: 'unhandled',
		nodes: unhandledNodes,
		prettify: false,
	};

	write(nodeContext);
}

/**
 * Changes the current context
 * @param {NodeContext} context
 * @param {string} newName
 */
function changeContext(context, newName) {
	write(context);

	const outputPath = outputPaths[newName];

	nodeContext = {
		name: newName,
		nodes: [],
		prettify: true,
	};

	if (outputPath) pathContext = outputPath;
}

/**
 * Writes the AST node context to disk.
 * @param {NodeContext} context
 */
function write(context) {
	if (context.nodes.length === 0) {
		consola.error(
			new Error(`No nodes handled for context '${context.name}'`)
		);
		process.exit(1);
	}

	if (context.name === context.name.toUpperCase()) {
		context.name = _.upperFirst(
			_.camelCase(context.name.split('_').join(' '))
		);
	}

	// Generate file name and paths.
	const outFile = context.name + '.js';
	const outPath = path.join(savePath, pathContext);
	const outFilePath = path.join(outPath, outFile);

	// Build a new AST from the nodes and generate the output code.
	const outAst = b.program(context.nodes);
	let outCode = recast.print(outAst).code;

	// Whether to prettify the output code for easier inspection.
	if (context.prettify) outCode = prettier.format(outCode, prettierConfig);

	fs.outputFileSync(outFilePath, outCode, 'utf-8');

	consola.success(
		chalk.green(
			`Wrote '${outFile}' to '${path.join('source', pathContext || '/')}'`
		)
	);
}
