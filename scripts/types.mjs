/**
 * @typedef {Object} NodeContext
 * @property {string} name
 * @property {any[]} nodes
 * @property {boolean} prettify
 */

/**
 * @typedef {(context: NodeContext, newName: string)} ChangeContext
 */

/**
 * Node handler function type
 * @typedef {(node: any, context: NodeContext, changeContext: ChangeContext) => boolean} NodeHandler
 */

/**
 * Condition handler type
 * @typedef {(node: any, nextNode: any, context: NodeContext) => string} ConditionHandler
 */
