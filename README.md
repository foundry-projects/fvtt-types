# Foundry VTT: Type Definitions

This is the new public repository for my personal library of type definitions for the Foundry Virtual Tabletop Software, starting with version 0.8.1.

It is meant to replace the original library, which is now superseded by the version managed by the League of Foundry Developers. I wanted to continue to manage my own type definitions for my projects, and the shift to the 0.8.x version of Foundry VTT means my original library has become deprecated.

The original library remains public, but development has ceased. Developers are free to use this library if they choose to, but I will only update it as I need, so it will see much slower development and less coverage than the League's library.

Though feedback and suggestions are welcome, I cannot accomodate requests.

## Code Splitter
This repository includes a code splitter script (`scripts/split.mjs`). This script will analyze `foundry.js` and split it into separate files for better inspection and diffing of changes from updates. The type definitions are not a 1:1 match with the split files, however.

To use the script, place `foundry.js` into the folder (it will be ignored by Git), and run `npm run split`, or just `node scripts/split.mjs`. This will generate the output under `source` (which is also ignored by Git).

**NOTE: Neither `foundry.js` nor the `source` folder should ever be included in the repository as they contain Foundry VTT source code.**

If you want to take advantage of diffing changes using Git and VSCode, you can put the generated source code in a private repository and overwrite the files with every update. ***Never, EVER, share this code without permission!***